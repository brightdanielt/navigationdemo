Navigation Demo
=============================
This is a sample to showcase navigation in compose

Main features
------------

- Navigating with arguments
- Nested navigation
- Navigating with BottomNavigationItem

Reference
-----------

- [Official guideline](https://developer.android.com/jetpack/compose/navigation?hl=zh-tw#setup)
- [Google Code-labs](https://developer.android.com/codelabs/jetpack-compose-navigation?hl=zh-tw#0)