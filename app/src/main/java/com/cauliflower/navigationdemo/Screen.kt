package com.cauliflower.navigationdemo

import androidx.annotation.StringRes

sealed class Screen(val route: String, @StringRes val resourceId: Int) {
    object ScreenA : Screen("screen_A", R.string.screen_A)
    object ScreenA1 : Screen("screen_A1", R.string.screen_A1)
    object ScreenA2 : Screen("screen_A2", R.string.screen_A2)
    object ScreenA3 : Screen("screen_A3", R.string.screen_A3)
    object ScreenA3_1 : Screen("screen_A3_1", R.string.screen_A3_1)
    object ScreenA3_2 : Screen("screen_A3_2", R.string.screen_A3_2)

    object ScreenB : Screen("screen_B", R.string.screen_B)
    object ScreenC : Screen("screen_C", R.string.screen_C)

    object ScreenProduct : Screen("screen_Product", R.string.screen_Product)
}