package com.cauliflower.navigationdemo

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import androidx.navigation.navigation
import com.cauliflower.navigationdemo.ui.theme.NavigationDemoTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NavigationDemoTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MyApp()
                }
            }
        }
    }

}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun MyApp() {
    val navController = rememberNavController()
    val items = listOf(Screen.ScreenA, Screen.ScreenB, Screen.ScreenC, Screen.ScreenProduct)

    Scaffold(
        bottomBar = {
            BottomNavigation {
                val navBackStackEntry by navController.currentBackStackEntryAsState()
                val currentDestination = navBackStackEntry?.destination
                items.forEach { screen ->
                    BottomNavigationItem(
                        icon = { Icon(Icons.Filled.Favorite, contentDescription = null) },
                        label = { Text(stringResource(screen.resourceId)) },
                        selected = currentDestination?.hierarchy?.any { it.route == screen.route } == true,
                        onClick = {
                            navController.navigate(screen.route) {
                                // Pop up to the start destination of the graph to
                                // avoid building up a large stack of destinations
                                // on the back stack as users select items
                                popUpTo(navController.graph.findStartDestination().id) {
                                    saveState = true
                                }
                                // Avoid multiple copies of the same destination when
                                // reselecting the same item
                                launchSingleTop = true
                                // Restore state when reselecting a previously selected item
                                restoreState = true
                            }
                        }
                    )
                }
            }
        }
    ) { innerPadding ->
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            NavHost(
                navController,
                startDestination = Screen.ScreenA.route,
                Modifier.padding(innerPadding)
            ) {
                // nested-nav
                screenAGraph(navController)
                composable(Screen.ScreenB.route) { ScreenB() }
                composable(Screen.ScreenC.route) { ScreenC() }

                composable(Screen.ScreenProduct.route) {
                    ScreenProducts { id ->
                        navController.navigate("product/$id")
                    }
                }

                // nav-with-args
                composable(
                    route = "product/{productId}",
                    arguments = listOf(navArgument("productId") {
                        type = NavType.StringType
                    })
                ) { backStackEntry ->
                    ProductContent(backStackEntry.arguments?.getString("productId"))
                }
            }
        }
    }
}

private fun NavGraphBuilder.screenAGraph(navController: NavHostController) {
    navigation(startDestination = Screen.ScreenA1.route, Screen.ScreenA.route) {

        composable(Screen.ScreenA1.route) {
            ScreenA_1(
                onNavigateA2 = {
                    navController.navigate(route = Screen.ScreenA2.route) {
                        popUpTo(Screen.ScreenA1.route)
                    }
                },
                onNavigateA3 = {
                    navController.navigate(route = Screen.ScreenA3.route) {
                        popUpTo(Screen.ScreenA1.route)
                    }
                }
            )
        }

        composable(Screen.ScreenA2.route) { ScreenA_2() }

        screenA3Graph(navController)
    }
}

private fun NavGraphBuilder.screenA3Graph(navController: NavHostController) {
    navigation(startDestination = Screen.ScreenA3_1.route, Screen.ScreenA3.route) {

        composable(Screen.ScreenA3_1.route) {
            ScreenA_3_1(onNavigateA3_2 = {
                navController.navigate(route = Screen.ScreenA3_2.route)
            })
        }

        composable(Screen.ScreenA3_2.route) { ScreenA_3_2() }
    }
}