package com.cauliflower.navigationdemo

data class Product(
    val id: String,
    val name: String,
    val price: String
)