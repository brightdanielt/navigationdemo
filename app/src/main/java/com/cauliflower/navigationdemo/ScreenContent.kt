package com.cauliflower.navigationdemo

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import kotlinx.coroutines.delay

@Composable
fun ScreenA_1(
    onNavigateA2: () -> Unit,
    onNavigateA3: () -> Unit
) {
    Column {
        Text("Screen A1")
        Button(onClick = onNavigateA2) { Text(text = "To A2") }
        Button(onClick = onNavigateA3) { Text(text = "To A3") }
    }
}

@Composable
fun ScreenA_2() {
    Column {
        Text("Screen A2")
    }
}

@Composable
fun ScreenA_3_1(
    onNavigateA3_2: () -> Unit
) {
    Column {
        Text("Screen A3_1")
        Button(onClick = onNavigateA3_2) { Text(text = "To A3_2") }
    }
}

@Composable
fun ScreenA_3_2() {
    Column {
        Text("Screen A3_2")
    }
}

@Composable
fun ScreenB() {
    Column {
        Text("Screen B")
    }
}

@Composable
fun ScreenC() {
    Column {
        Text("Screen C")
    }
}

@Composable
fun ScreenProducts(onNavigateProduct: (String) -> Unit) {
    Column {
        Button(onClick = { onNavigateProduct("001") }) {
            Text(text = "product 001")
        }
        Button(onClick = { onNavigateProduct("002") }) {
            Text(text = "product 002")
        }
        Button(onClick = { onNavigateProduct("003") }) {
            Text(text = "product 003")
        }
    }
}

@Composable
fun ProductContent(productId: String?) {
    var product: Product? by remember {
        mutableStateOf(null)
    }
    LaunchedEffect(key1 = productId) {
        productId?.let {
            // simulate loading product info from data source
            product = fetchProduct(productId)
        }
    }

    product?.let {
        Column {
            Text(text = "id:${it.id}")
            Text(text = "name:${it.name}")
            Text(text = "price:${it.price}")
        }
    } ?: CircularProgressIndicator()
}

suspend fun fetchProduct(productId: String): Product {
    delay(1000L)
    return when (productId) {
        "001" -> Product(productId, "001", "100")
        "002" -> Product(productId, "002", "200")
        "003" -> Product(productId, "003", "300")
        else -> throw Exception("Product id $productId not exist")
    }
}